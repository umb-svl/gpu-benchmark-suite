# Kernel generation tool

Generates a kernel compatible with multiple verification tools.

By default, it generates a GPUVerify compatible kernel.

```bash
$ ./kernel-gen.py examples/hello.yaml
```

We can set the tool compatibility with `--tool`, or simply `-t`. We can also use
the `-o` parameter to set the output filename (by default it sends the data to
the standard output). For instance, we can generate the example to be compatible
with PUG using the following command:

```bash
$ ./kernel-gen.py examples/hello.yaml -t pug -o main.c
$ ../../tools/pug-v0.2_x64/pug main.c
```

## Kernel specification

A kernel specification is a YAML file with the following fields:

 - `arrays`: declare the kernel parameters that are shared arrays, a list of dictionaries, each contains the name and the parameter type.
 - `body`: the kernel code.
 - `pass`: optional parameter declaring if we expect the example to pass or fail verification. Defaults to passing verification.

```yaml
arrays:
  - {p: int}
pass: true # defaults to true, may be omitted
body: |
    p[threadIdx.x] = p[threadIdx.x + 1];
```
# Micro-benchmark generation

The usage of `microbench-gen.py` is the same as for `kernel-gen` except that it
adds a new parameter `--size` to set the problem size.

The following example generates 100 serial loops in file `main.c` compatible
with PUG.

```
$ ./microbench-gen.py serial-loops --size 100 -o main.c -t pug
$ ../../tools/pug-v0.2_x64/pug main.c
```
