{% extends "main.base" %}
{% set arrays = [
    {"name": "out", "type": "float", "size": array_size},
    {"name": "a", "type": "float", "size": array_size},
    {"name": "b", "type": "float", "size": array_size},
    ]
%}

{% block body %}
    {%- for idx in range(size) %}
    if (threadIdx.x == {{idx}}) {
        out[threadIdx.x] = a[{{idx}}] + b[{{idx}}];
    }
    {%- endfor %}
{% endblock %}
