#!/usr/bin/env python3

import sys
import pprint
import argparse
import math
import subprocess
import time
import re
import yaml
import matplotlib.pyplot as plt
from pandas.plotting import table
from matplotlib.lines import Line2D
from matplotlib import rc
import psutil
import pandas as pd
import numpy as np
from os import path
import os
import csv
from scipy import stats
from scipy.optimize import curve_fit

# -------------------- = -------------------- #
microbenchmark_variables = []
# Microbenchmark timeout
microbenchmark_timeout = 90
# Colors
colors = [('#004D40','s','solid'),
    ('#1E88E5','o','dotted'),
    ('#D81B60','D','dashed'),
    ('orange','^',(0, (5, 10))),
    ('cyan','v','dashdot'),
    ('magenta','X','solid'),
    ('red','P','solid')
    ]

pp = pprint.PrettyPrinter(indent=4)
rc('text', usetex=True)
# -------------------- = -------------------- #

def create_microbenchmark_chart(tools_dict,tools,groups,colors):
    r_value_time_dict = {}
    r_value_mem_dict = {}
    for micro_bm in tools_dict:
        r_value_time_dict[micro_bm] = {}
        r_value_mem_dict[micro_bm] = {}
    range_start = groups[0]
    range = "-".join(list(map(lambda x : str(x),groups)))
    title_size,subtitle_size = 13,10  # Font size of titles (supertitle & subtitle)
    subplot_idx = 0
    timeout_annot_offset,timeout_time_pos = -microbenchmark_timeout*0.15,microbenchmark_timeout
    num_subplots = len(tools_dict)

    legend_t = [ i for i in tools ]
    legend_c = colors[:len(legend_t)]

    # Time chart init
    fig_dimensions = (5, 12)    #width,height
    fig = plt.figure(figsize=fig_dimensions)
    fig.tight_layout()
    #fig.suptitle('Micro-benchmark Verification Time of CUDA Kernels ('+range+')',fontsize=title_size)
    #fig.text(0.5, 0.03, 'Degree of microbenchmark feature', ha='center', va='center')
    fig.text(0.02, 0.5, 'Time (s)', ha='center', va='center', rotation='vertical',fontsize=14)
    fig.text(0.5, 0.01, 'Problem size', ha='center', va='center', rotation='0',fontsize=14)
    # Memory chart init
    fig2 = plt.figure(figsize=fig_dimensions)
    fig2.tight_layout()
    #fig2.suptitle('Micro-benchmark Verification Memory Usage of CUDA Kernels ('+range+')',fontsize=title_size)
    #fig2.text(0.5, 0.03, 'Degree of microbenchmark feature', ha='center', va='center')
    fig2.text(0.98, 0.5, 'Memory usage (MB)', ha='center', va='center', rotation='270',fontsize=14)
    fig2.text(0.5, 0.01, 'Problem size', ha='center', va='center', rotation='0',fontsize=14)
    # Separate charts by micro-benchmark.
    for micro_bm in tools_dict:
        color_idx,timeout_annot = 0,1
        subplot_idx += 1
        # Title for subplots corresponding to micro-benchmarks
        ax = fig.add_subplot(num_subplots,1,subplot_idx)
        ax.set_title(micro_bm,fontsize=15, x=0.13, y=0.84, ha='left',\
            bbox={'boxstyle': 'round', 'pad': 0.18, 'facecolor': 'blue', 'edgecolor': 'black', 'alpha': 0.05},)
        ax.tick_params(axis='both', which='major', labelsize=9)
        ax2 = fig2.add_subplot(num_subplots,1,subplot_idx)
        ax2.yaxis.tick_right()
        ax2.set_title(micro_bm,fontsize=15, x=0.13, y=0.84, ha='left',\
            bbox={'boxstyle': 'round', 'pad': 0.18, 'facecolor': 'blue', 'edgecolor': 'black', 'alpha': 0.05},)
        ax2.tick_params(axis='both', which='major', labelsize=9)
        # Need max x-axis ticks to properly set up grid
        x_pos_max=[]
        x_pos2_max=[]
        timeout_mem_max, timeout_mem_min= -math.inf,math.inf
        for tool in tools_dict[micro_bm]:
            k_time = list(map( (lambda x : x[1]) , tools_dict[micro_bm][tool] ))
            k_mem = list(map( (lambda x : x[2]) , tools_dict[micro_bm][tool] ))
            timeout_cutoff = next((k_time.index(x) for x in k_time if (x>=microbenchmark_timeout)),None)
            k_mem= k_mem[:timeout_cutoff]
            if timeout_cutoff != 0:
                mx_mem,mn_mem = max(k_mem),min(k_mem)
                if mx_mem > timeout_mem_max:
                    timeout_mem_max = mx_mem
                if mn_mem < timeout_mem_min:
                    timeout_mem_min = mn_mem
        for tool in tools_dict[micro_bm]:
            k_time = list(map( (lambda x : x[1]) , tools_dict[micro_bm][tool] ))
            k_mem = list(map( (lambda x : x[2]) , tools_dict[micro_bm][tool] ))
            # Cutoff the remaining data when the plot reaches timeout
            # Leaving the last point at timeout.
            timeout_cutoff = next((k_time.index(x) for x in k_time if (x>=microbenchmark_timeout)),None)
            if timeout_cutoff is not None:
                k_time = k_time[:timeout_cutoff]
                k_mem= k_mem[:timeout_cutoff]
                if timeout_cutoff==0:
                    annotation_offset_time = timeout_time_pos * (1 - 0.4 * timeout_annot) * 0.3
                    annotation_offset_mem = (0.92-0.15 * timeout_annot)* (timeout_mem_max-timeout_mem_min) + timeout_mem_min
                    if timeout_annot == 1 :
                        # Timeout arrows on Micro-benchmark graphs
                        ax.annotate(tool, xy=(timeout_cutoff+1, timeout_time_pos), xytext=(timeout_cutoff+1, annotation_offset_time), ha = 'center',\
                            bbox={'boxstyle': 'round', 'pad': -0.05, 'facecolor': 'blue', 'edgecolor': 'blue', 'alpha': 0},\
                            arrowprops={'arrowstyle':"wedge,tail_width=0.5", 'alpha':0.3, 'color': 'black'})
                        ax2.annotate(tool, xy=(timeout_cutoff+1, timeout_mem_max), xytext=(timeout_cutoff+1, annotation_offset_mem), ha = 'center',\
                            bbox={'boxstyle': 'round', 'pad': -0.05, 'facecolor': 'blue', 'edgecolor': 'blue', 'alpha': 0},\
                            arrowprops={'arrowstyle':"wedge,tail_width=0.5", 'alpha':0.3, 'color': 'black'})
                    else:
                        ax.annotate(tool, xy=(timeout_cutoff+1, timeout_time_pos), xytext=(timeout_cutoff+1, annotation_offset_time), ha = 'center')
                        ax2.annotate(tool, xy=(timeout_cutoff+1, timeout_mem_max), xytext=(timeout_cutoff+1, annotation_offset_mem), ha = 'center')
                    timeout_annot += 1
            elif (tool == 'pug' and micro_bm == 'accesses'):
                annotation_offset_time = 60 * (1 - 0.4 * 1) * 0.3
                annotation_offset_mem = (0.92-0.15 * 1)* (timeout_mem_max-timeout_mem_min) + timeout_mem_min
                ax.annotate(tool, xy=(0+1, 60), xytext=(0+1, annotation_offset_time), ha = 'center',\
                    bbox={'boxstyle': 'round', 'pad': -0.05, 'facecolor': 'blue', 'edgecolor': 'blue', 'alpha': 0},\
                    arrowprops={'arrowstyle':"wedge,tail_width=0.5", 'alpha':0.3, 'color': 'black'})
                ax2.annotate(tool, xy=(0+1, timeout_mem_max), xytext=(0+1, annotation_offset_mem), ha = 'center',\
                    bbox={'boxstyle': 'round', 'pad': -0.05, 'facecolor': 'blue', 'edgecolor': 'blue', 'alpha': 0},\
                    arrowprops={'arrowstyle':"wedge,tail_width=0.5", 'alpha':0.3, 'color': 'black'})
            # Setting up x-axis
            x_pos = np.arange(range_start,range_start+(len(k_time)))
            x_pos2 = np.arange(range_start,range_start+(len(k_mem)))
            x_pos_max = x_pos if (len(x_pos) > len(x_pos_max)) else x_pos_max
            x_pos2_max = x_pos2 if (len(x_pos2) > len(x_pos2_max)) else x_pos2_max
            if tool=='faial':
                sc_linewidth = 2.5
                #sc_marker = legend_c[color_idx][1]
                sc_linestyle = legend_c[color_idx][2]
                sc_marker = None
            else:
                sc_linewidth = 2.5
                #sc_marker = legend_c[color_idx][1]
                sc_linestyle = legend_c[color_idx][2]
                sc_marker = None
            if not (tool == 'pug' and micro_bm == 'accesses'):
                ax.plot(x_pos,k_time,marker=sc_marker,markeredgecolor='black',markeredgewidth=0.5,\
                    linewidth=sc_linewidth,markersize=5,color=legend_c[color_idx][0],linestyle=sc_linestyle,\
                    zorder=(len(tools_dict[micro_bm])-color_idx)*2)
                ax2.plot(x_pos2,k_mem,marker=sc_marker,markeredgecolor='black',markeredgewidth=0.5,\
                    linewidth=sc_linewidth,markersize=5,color=legend_c[color_idx][0],linestyle=sc_linestyle,\
                    zorder=(len(tools_dict[micro_bm])-color_idx)*2)
            ax.grid(True,axis='both',linestyle='--',linewidth=0.5)
            ax2.grid(True,axis='both',linestyle='--',linewidth=0.5)
            # Y-axis to log
            ax.set_yscale('log')
            ax2.set_yscale('linear')
            # Add horizontal 'timeout' line
            #ax.axhline(y=90,color='r',linestyle='-')
            #ax2.axhline(y=90,color='r',linestyle='-')
            # Set x-axis ticks only if there's data
            if (len(x_pos) > 0):
                ax.set_xticks(x_pos_max[::7])
            if (len(x_pos2) > 0):
                ax2.set_xticks(x_pos2_max[::7])
            # x-axis labels only at the most-bottom plot
            if not (subplot_idx==num_subplots):
                ax.set_xticklabels([])
                ax.tick_params(axis=u'x', which=u'both',length=0)
            if not (subplot_idx==num_subplots):
                ax2.set_xticklabels([])
                ax2.tick_params(axis=u'x', which=u'both',length=0)


            color_idx += 1

    # Custom legend
    custom_legend = list(map(
        (lambda x : (Line2D([0],[0],marker=None,markeredgecolor='black',linestyle=x[2],markersize=10,color=x[0]))),
        legend_c))
    # Add timeout to legend
    #custom_legend.append((Line2D([0],[0],linestyle='-',color='r')))
    #legend_t.append('Timeout')
    export_legend('microbenchmark-legend.pdf',custom_legend,legend_t,ncol=6)
    fig.subplots_adjust(left=0.09,right=0.99,top=0.99,bottom=0.045,hspace=0.03)
    #fig2.legend(custom_legend,legend_t,loc='upper center',ncol=5)
    fig2.subplots_adjust(left=0.01,right=0.91,top=0.99,bottom=0.045,hspace=0.03)

    # Save the graph figure
    png_name_time = 'Micro-benchmark-time' + '-' + range + '.pdf'
    png_name_mem = 'Micro-benchmark-memory' + '-' + range + '.pdf'

    if os.path.isfile(png_name_time):
        os.remove(png_name_time)
    if os.path.isfile(png_name_mem):
        os.remove(png_name_mem)

    fig.savefig(png_name_time,dpi=150)
    plt.close(fig)
    fig2.savefig(png_name_mem,dpi=150)
    plt.close(fig2)


def export_legend(name,handles,labels,ncol=0,loc='center',fontsize='xx-large',framealpha=0.2):
    figlegend = plt.figure()
    figlegend.legend(handles,labels,ncol=ncol,loc=loc,fontsize=fontsize,framealpha=framealpha)
    figlegend.savefig(name,bbox_inches='tight',dpi=100)

def create_pie_chart(tools_dict,k_race):
    def get_sizes(arr):
        r_drf,r_racy,r_err,r_timeout = 0,0,0,0
        for a in arr:
            if a=='racy':
                r_racy += 1
            elif a=='drf':
                r_drf += 1
            elif a=='timeout':
                r_timeout += 1
            elif 'error' in a:
                r_err += 1
            else:
                r_err += 1
        n = len(arr)
        return [r_drf,
                r_racy,
                r_err,
                r_timeout,]

    tools = [x[0] for x in k_race]
    labels = ['Correct','False Alarm','Unsupported','Timeout']
    sizes = list(map((lambda x : get_sizes(x)),[y[1:] for y in k_race]))
    colors = ['#99ff99','#66b3ff','#ffcc99','#ff6666']
    # colors = ['#99ff99','#66b3ff','#ff9999','#F66D44']
    explode = [0,0,0,0]


    for i in range(len(tools)):
        def make_autopct(values):
            def my_autopct(pct):
                total = sum(values)
                val = int(round(pct*total/100.0))
                pct = int(pct)
                return f"{pct:d}\% ({val:d})"
            return my_autopct

        cur_labels,cur_sizes,cur_colors,cur_explode = labels[:],sizes[:],colors[:],explode[:]
        total_size = sum(cur_sizes[i])

        cur_tool = tools[i]
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.pie(cur_sizes[i],pctdistance=0.9,autopct='',\
                colors=cur_colors, startangle=-40,textprops={'fontsize': 26})
        centre_circle = plt.Circle((0,0),0.5,fc='white')
        for ii in range(len(cur_sizes[i])):
            label_text = "{}: {:2.1f}\% ({})".format(labels[ii][0],cur_sizes[i][ii]/total_size*100,cur_sizes[i][ii])
            fig.text(0.19, 0.83-(ii*0.1), label_text, ha='left', va='top', rotation='horizontal',fontsize=22)
        fig = plt.gcf()
        #plt.legend(cur_labels, loc="lower center",ncol=4)
        fig.gca().add_artist(centre_circle)
        # Equal aspect ratio ensures that pie is drawn as a circle
        ax.axis('equal')
        fig.savefig(cur_tool+'-stats.pdf',pad_inches=0.1,bbox_inches='tight')
    new_labels = [ x+" ("+x[0]+")" for x in labels]
    custom_legend = [(Line2D([0],[0],marker='s',\
        linestyle="None",markersize=10,color=colors[ii])) for ii in range(len(labels))]
    export_legend('stats-legend.pdf',custom_legend,new_labels ,ncol=4)

def create_relation_chart(tools_dict,colors,baseline = 'gpuverify'):
    def percent_diff(n1,n2):
        return (n2-n1) / ((n2+n1)/2) * 100
    def percent_change(n1,n2):
        return ((n2-n1)/n1) * 100
    def change(n1,n2):
        if n2 > n1:
            return n2/n1 + 1
        elif n2 < n1:
            return 1 - n2/n1
        if n2==n1:
            return 0
    def remove_unsupported(k_t,k_m,k_r,k_f):
        k_arr = np.asarray(k_r).T
        col_delete = []
        for col in range(len(k_arr)):
            if not any(['drf' in y for y in k_arr[col]]):
                col_delete.append(col)
        for i in reversed(col_delete[1:]):
            for n in range(len(k_t)):
                k_t[n].pop(i)
                k_m[n].pop(i)
                k_r[n].pop(i)
            k_f.pop(i-1)
        for i in range(len(k_r)):
            mask_i = [j!='drf' for j in k_r[i] ]
            mask_i[0] = False
            # Time cutoff 10, memory cutoff 100
            for ii in range(1,len(k_t[i][1:])+1):
                if k_t[i][ii] > 10:
                    mask_i[ii] = True
            for ii in range(1,len(k_m[i][1:])+1):
                if k_m[i][ii] > 100:
                    mask_i[ii] = True
            k_t[i] = np.ma.array(k_t[i],mask=mask_i)
            k_m[i] = np.ma.array(k_m[i],mask=mask_i)
            k_t[i] = np.ma.filled(k_t[i][1:].astype(float), np.nan)
            k_m[i] = np.ma.filled(k_m[i][1:].astype(float), np.nan)
        return k_t,k_m,k_f
    def shortenFilename(k_f,baseline):
        for idx in range(len(k_f)):
            k_f[idx] = k_f[idx].replace(baseline+'/','')
        return k_f

    k_filename,k_time,k_mem,k_race = [],[],[],[]
    for tool in tools_dict:
        # 'filename', 'time', 'memory', 'data_races'
        #k_filename = list(map( (lambda x : x[0]) , tools_dict[tool] ))
        k_time.append([tool]+list(map( (lambda x : x[1]) , tools_dict[tool] )))
        k_mem.append([tool]+list(map( (lambda x : x[2]) , tools_dict[tool] )))
        k_race.append([tool]+list(map( (lambda x : x[3]) , tools_dict[tool] )))
    assert([x[0] for x in k_time]==[x[0] for x in k_mem]==[x[0] for x in k_mem]),"Tools are not in the same order"
    assert(len(k_time)==len(k_mem)==len(k_race)),"Data points are not of same length"
    # Swap baseline to index 0
    baseline_index = [x[0] for x in k_time].index(baseline)
    k_time[0],k_time[baseline_index] = k_time[baseline_index],k_time[0]
    k_mem[0],k_mem[baseline_index] = k_mem[baseline_index],k_mem[0]
    k_race[0],k_race[baseline_index] = k_race[baseline_index],k_race[0]
    assert(k_time[0][0]==k_mem[0][0]==baseline==k_race[0][0])
    for i in reversed(range(len(k_time))):
        for n in range(1,len(k_time[i])):
            # percent change compared to baseline
            #k_time[i][n] = change(k_time[i][n],k_time[0][n])
            #k_mem[i][n] = change(k_mem[i][n],k_mem[0][n])
            pass
    k_filename = list(map( (lambda x : x[0]) , tools_dict[baseline] ))

    # Start pie plot
    create_pie_chart(tools_dict,k_race)
    # Remove Unsupported
    legend_toolname = [x[0] for x in k_time]
    k_time,k_mem,k_filename = remove_unsupported(k_time,k_mem,k_race,k_filename)
    k_filename = shortenFilename(k_filename,baseline)
    #toTxtFile(k_filename,baseline)
    # Start plot
    fig = plt.figure(figsize=(20, 10))
    #fig.tight_layout()
    ax1 = fig.add_subplot(2,1,1)
    #ax1.set_ylim(-2,70)    ##### HARD CODED LIMIT. TODO: CHANGE #####
    ax1.margins(x=0.01)
    ax2 = fig.add_subplot(2,1,2)
    ax2.margins(x=0.01)
    x_pos = range(1,len(k_filename)+1)
    ax1.set_yscale('symlog')
    ax2.set_yscale('linear')
    ax1.grid(True,axis='both',linestyle='--',linewidth=0.5)
    ax2.grid(True,axis='both',linestyle='--',linewidth=0.5)
    for i in range(len(k_time)):
        masked_k_time = k_time[i]
        masked_k_mem = k_mem[i]
        ax1.scatter(x_pos,masked_k_time,marker=colors[i][1],s=15.5,c=colors[i][0])
        # markerline, stemlines, base = ax1.scatter(x_pos,k_time[i][1:],markerfmt='none',bottom=0.0,linefmt=colors[i][0],use_line_collection=True)
        # markerline.set_markerfacecolor('none')
        # stemlines.set_linewidth(3)
        ax2.scatter(x_pos,masked_k_mem,marker=colors[i][1],s=15.5,c=colors[i][0])
        #markerline, stemlines, base = ax2.scatter(x_pos,k_mem[i][1:],markerfmt='none',bottom=0.0,linefmt=colors[i][0],use_line_collection=True)
        # markerline.set_markerfacecolor('none')
        # stemlines.set_linewidth(3)
    #ax1.axhline(y=0,color='black',linestyle=(0, (1, 1)),linewidth=1.8,alpha=0.3)
    #ax1.axhline(y=1,color='r',linewidth=1.8,alpha=0.3)
    relation_ylabel_fontsize = 22
    ax1.set_ylabel('Time (s)',fontsize=relation_ylabel_fontsize)
    ax1.set_xlabel('Kernel id',fontsize=relation_ylabel_fontsize)

    ax2.set_ylabel('Memory (MB)',fontsize=relation_ylabel_fontsize)
    ax2.set_xlabel('Kernel id',fontsize=relation_ylabel_fontsize)
    # Remove x ticks
    ax1.set_xticklabels([])
    #ax1.tick_params(axis=u'x', which=u'both',length=0)
    ax1.xaxis.set_ticks(np.arange(0, len(k_filename), 20))
    ax2.xaxis.set_ticks(np.arange(0, len(k_filename), 20))
    #ax2.tick_params(axis=u'x', which=u'both',length=0)
    fig.text(0.02, 0.5, '', ha='center', va='center', rotation='vertical',fontsize=19)
    # Custom legend
    custom_legend = list(map(
        (lambda x : (Line2D([0],[0],marker=x[1],markeredgecolor='black',linestyle="None",markersize=10,color=x[0]))),
        colors[:len(k_time)]))
    #custom_legend[0] = (Line2D([0],[0],color='r',linewidth=1.8,alpha=0.3))
    fig.subplots_adjust(left=0.04,right=0.98,top=0.99,bottom=0.055,hspace=0.03)

    strFile = 'time-relation-'+baseline+'-scatter.pdf'
    if os.path.isfile(strFile):
        os.remove(strFile)
    fig.savefig(strFile,dpi=150)
    export_legend('time-relation-legend.pdf',custom_legend,legend_toolname,fontsize='xx-large',framealpha=0.2,ncol=3)


def init_tool_dict(tools):
    tools_dict = {}
    mb_vars = ["accesses","barriers","conditionals","unsync-loops","sync-loops"]
    for bm in mb_vars:
        tools_dict[bm] = {}
        for t in tools:
            tools_dict[bm][t] = []
    return tools_dict



def sort_alphanum( l ):
    convert = lambda t: int(t) if t.isdigit() else t
    alphanum_filename = lambda x: [convert(c) for c in re.split('([0-9]+)', x[0])]
    return sorted(l, key = alphanum_filename)

def find_microbenchmark_group(name):
    if "loops" in name:
        if "nested" in name:
            if "sync" in name:
                return "sync-loops"
            else:
                return "unsync-loops"
        elif "serial" in name:
            return "serial-loops"
    elif "barriers" in name:
        return "barriers"
    elif "ifs" in name:
        return "conditionals"
    elif "accs" in name:
        return "accesses"

def csv_to_dict(microbenchmark_variables,tools,groups,csvfile):
    if len(groups)!=0:
        tools_dict = init_tool_dict(tools)
        for mb_vars in microbenchmark_variables:
            for tool in tools:
                csv_filename = csvfile.replace("<toolname>",tool+'-'+mb_vars)
                #print("Reading :\t "+str(csv_filename))
                # Add filename and time to tools dictionary.
                if (path.exists(csv_filename)):
                    df = pd.read_csv(csv_filename,usecols=['time','filename','memory'])
                    calc_mean = ([],[])
                    old_data = ''
                    for idx,row in df.iterrows():
                        data = (row['filename'],row['time'],row['memory'])
                        bm_group = find_microbenchmark_group(data[0])
                        if old_data==data[0] or old_data=='':
                            calc_mean[0].append(data[1])
                            calc_mean[1].append(data[2])
                        else:
                            n1,n2 = np.mean(calc_mean[0]),np.mean(calc_mean[1])
                            if bm_group != 'serial-loops':
                                tools_dict[bm_group][tool].append((data[0],n1,n2))
                            calc_mean = ([],[])
                            calc_mean[0].append(data[1])
                            calc_mean[1].append(data[2])
                        if idx == df.index[-1]:
                            n1,n2 = np.mean(calc_mean[0]),np.mean(calc_mean[1])
                            tools_dict[bm_group][tool].append((data[0],n1,n2))
                        old_data = data[0]
                    # Sort each tool's benchmark group within the dict.
                    for k in tools_dict:
                        tools_dict[k][tool]= sort_alphanum(tools_dict[k][tool])
                else:
                    print("ERROR: "+str(csv_filename)+" does not exist.")
        return tools_dict
    else:
        tools_dict = {}
        for tool in tools:
            tools_dict[tool] = []
            csv_filename = csvfile.replace("<toolname>",tool)
            print("Reading :\t "+str(csv_filename))
            # Add filename and time to tools dictionary.
            if (path.exists(csv_filename)):
                df = pd.read_csv(csv_filename,usecols=['time','filename','memory','data_races'])
                calc_mean = ([],[])
                old_data = ''
                for idx,row in df.iterrows():
                    data = (row['filename'],row['time'],row['memory'],row['data_races'])
                    if old_data==data[0] or old_data=='':
                        calc_mean[0].append(data[1])
                        calc_mean[1].append(data[2])
                    else:
                        n1,n2 = np.mean(calc_mean[0]),np.mean(calc_mean[1])
                        tools_dict[tool].append((data[0],n1,n2,data[3]))
                        calc_mean = ([],[])
                        calc_mean[0].append(data[1])
                        calc_mean[1].append(data[2])
                    if idx == df.index[-1]:
                        n1,n2 = np.mean(calc_mean[0]),np.mean(calc_mean[1])
                        tools_dict[tool].append((data[0],n1,n2,data[3]))
                    old_data = data[0]
                # Sort each tool's benchmark group within the dict.
                for k in tools_dict:
                    tools_dict[k]= sort_alphanum(tools_dict[k])
            else:
                print("ERROR: "+str(csv_filename)+" does not exist.")
        return tools_dict

def get_groups():
    tools = []
    groups = []
    # Only start recording ranges after the "groups:" keyword is seen.
    group_seen = False
    with open('config.yaml', 'r') as file:
        data = yaml.load(file, Loader=yaml.FullLoader)
    for p in data['problems']:
        microbenchmark_variables.append(p)
    tools = list(data["tools"].keys())
    # Ignore Simulee
    if 'simulee' in tools:
        tools.remove('simulee')
    groups = data["default_range"]
    return tools,groups



def main(arg):

    parser = argparse.ArgumentParser(description='Graphs the verifcation time \
        given a CSV file generated by run-benchmark.py, \
        and a TXT file containing the dataset kernel names.')
    parser.add_argument('--csv-file',type=str, default='./timings-<toolname>.csv',\
        help='Specify relative path of .csv file containing kernel \
        verification times generated by run-benchmark.py. ')
    parser.add_argument('-mb','--micro-benchmark',action='store_true',help='Sets this \
        script to graph micro-benchmark data.')
    parser.add_argument('-rw','--real-world',action='store_true',help='Sets this \
        script to graph real-world (CAV) data.')
    args = parser.parse_args()
    csvfile = args.csv_file
    if args.micro_benchmark:
        print("Graphing :\t micro-benchmark data.")
        tools,groups = get_groups()
        # Prepares data for graphing.
        tools_dict = csv_to_dict(microbenchmark_variables,tools,groups,csvfile)
        create_microbenchmark_chart(tools_dict,tools,groups,colors)
    if args.real_world:
        print("Graphing :\t real-world (CAV) data.")
        tools,_ = get_groups()
        tools_dict = csv_to_dict(microbenchmark_variables,tools,{},csvfile)
        #create_relation_chart(tools_dict,baseline='gpuverify')
        create_relation_chart(tools_dict,colors,baseline='faial')


if __name__ == '__main__':
  main(sys.argv[1:])
