# /usr/bin/env python3

import csv
from dataclasses import dataclass


@dataclass
class Cost:
    cost: str
    exit_code: int


def load_cost(fp):
    files = {}
    for row in csv.DictReader(fp):
        cost = Cost(cost=row["cost"], exit_code=int(row["exit_code"]))
        files[row['filename']] = cost
    return files


def main(faial_csv="bc_results.csv", racuda_csv="racuda_results.csv"):
    with open(faial_csv) as fp:
        faial = load_cost(fp)
    with open(racuda_csv) as fp:
        racuda = load_cost(fp)

    for filename in sorted(faial):
        cost_f = faial[filename]
        cost_r = racuda[filename]
        # Only consider files that faial is able to handle and racuda is able to handle
        if cost_f.exit_code == 0 and cost_r.cost != '':
            # If both agree the cost is zero, we skip it as it is uninteresting
            if cost_f.cost == '0' and cost_r.cost == '0':
                continue
            # For now, we skip elements that racuda times out
            if cost_r.cost == 'timed out':
                continue
            # Show me the cost of all left
            print(filename)
            print("\tfaial:", cost_f.cost)
            print("\tracuda:", cost_r.cost)


main()
