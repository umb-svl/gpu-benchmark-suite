//pass
//--blockDim=[64,1,1] --gridDim=[64,1,1]

extern __attribute__((device)) int __dummyd_Histogram_w();
extern __attribute__((device)) int __dummyd_PartialHistograms_w();
extern __attribute__((device)) int __dummydata_w();
__global__ void kernel(int *__dummy, int *d_Histogram, int *d_PartialHistograms, int blockN)
{
    __shared__ int data[64];
    int __dummyd_Histogram;
    int __dummyd_PartialHistograms;
    int __dummydata;
    for (int i = 0; i < blockN; i += 64) {
        if (i >= threadIdx.x && i < blockN) {
            __dummyd_PartialHistograms = d_PartialHistograms[blockIdx.x + (64 * i)];
        }
    }
    data[threadIdx.x] = __dummydata_w();
    for (int stride = 1; stride < 33; stride *= 2) {
        __syncthreads();
        if (threadIdx.x < stride) {
            __dummydata = data[threadIdx.x];
            __dummydata = data[threadIdx.x + stride];
            data[threadIdx.x] = __dummydata_w();
        }
    }
    if (threadIdx.x == 0) {
        __dummydata = data[0];
        d_Histogram[blockIdx.x] = __dummyd_Histogram_w();
    }
}
