//pass
//--blockDim=[128,1,1] --gridDim=[128,1,1]

extern __attribute__((device)) int __dummyg_data_w();
extern __attribute__((device)) int __dummyuni_w();
extern __attribute__((device)) int __dummyuniforms_w();
__global__ void kernel(int *__dummy, int *g_data, int *uniforms, int baseIndex, int blockOffset, int n)
{
    __shared__ int uni[1];
    int __dummyg_data;
    int __dummyuni;
    int __dummyuniforms;
    if (threadIdx.x == 0) {
        __dummyuniforms = uniforms[blockIdx.x + blockOffset];
        uni[0] = __dummyuni_w();
    }
    __syncthreads();
    __dummyg_data = g_data[((256 * blockIdx.x) + baseIndex) + threadIdx.x];
    __dummyuni = uni[0];
    g_data[((256 * blockIdx.x) + baseIndex) + threadIdx.x] = __dummyg_data_w();
    __dummyg_data = g_data[128 + (((256 * blockIdx.x) + baseIndex) + threadIdx.x)];
    __dummyuni = uni[0];
    g_data[128 + (((256 * blockIdx.x) + baseIndex) + threadIdx.x)] = __dummyg_data_w();
}
