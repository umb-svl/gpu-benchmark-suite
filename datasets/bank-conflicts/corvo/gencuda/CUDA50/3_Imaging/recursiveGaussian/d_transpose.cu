//pass
//--blockDim=[32,32,1] --gridDim=[32,32,1]

extern __attribute__((device)) int __dummyblock_w();
extern __attribute__((device)) int __dummyidata_w();
extern __attribute__((device)) int __dummyodata_w();
__global__ void kernel(int *__dummy, int *idata, int *odata, int height, int width)
{
    __shared__ int block[272];
    int __dummyblock;
    int __dummyidata;
    int __dummyodata;
    if ((16 * blockIdx.x) + threadIdx.x < width && (16 * blockIdx.y) + threadIdx.y < height) {
        __dummyidata = idata[(((16 * blockIdx.y) + threadIdx.y) * width) + ((16 * blockIdx.x) + threadIdx.x)];
        block[(17 * threadIdx.y) + threadIdx.x] = __dummyblock_w();
    }
    __syncthreads();
    if ((16 * blockIdx.y) + threadIdx.x < height && (16 * blockIdx.x) + threadIdx.y < width) {
        __dummyblock = block[(17 * threadIdx.x) + threadIdx.y];
        odata[(((16 * blockIdx.x) + threadIdx.y) * height) + ((16 * blockIdx.y) + threadIdx.x)] = __dummyodata_w();
    }
}
