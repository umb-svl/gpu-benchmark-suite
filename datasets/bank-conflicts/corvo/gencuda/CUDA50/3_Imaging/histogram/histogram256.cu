//pass
//--blockDim=[192,1,1] --gridDim=[240,1,1]

extern __attribute__((device)) int __dummyd_Data_w();
extern __attribute__((device)) int __dummyd_PartialHistograms_w();
extern __attribute__((device)) int __dummys_Hist_w();
__global__ void kernel(int *__dummy, int *d_Data, int *d_PartialHistograms, int dataCount)
{
    __shared__ int s_Hist[1536];
    int __dummyd_Data;
    int __dummyd_PartialHistograms;
    int __dummys_Hist;
    for (int i = 0; i < 8; i += 1) {
        s_Hist[threadIdx.x + (192 * i)] = __dummys_Hist_w();
    }
    __syncthreads();
    for (int pos = 192 * blockIdx.x; pos < dataCount; pos += 46080) {
        if (pos >= (192 * blockIdx.x) + threadIdx.x && pos < dataCount) {
            __dummyd_Data = d_Data[pos];
        }
    }
    __syncthreads();
    for (int bin = 0; bin < 256; bin += 192) {
        if (bin >= threadIdx.x && bin < 256) {
            for (int i1 = 0; i1 < 6; i1 += 1) {
                __dummys_Hist = s_Hist[bin + (256 * i1)];
            }
            d_PartialHistograms[(256 * blockIdx.x) + bin] = __dummyd_PartialHistograms_w();
        }
    }
}
