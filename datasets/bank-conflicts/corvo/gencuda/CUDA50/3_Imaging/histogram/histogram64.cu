//pass
//--blockDim=[64,1,1] --gridDim=[4370,1,1]

extern __attribute__((device)) int __dummyd_Data_w();
extern __attribute__((device)) int __dummyd_PartialHistograms_w();
extern __attribute__((device)) int __dummys_Hist_w();
__global__ void kernel(int *__dummy, int *d_Data, int *d_PartialHistograms, int dataCount)
{
    __shared__ int s_Hist[4096];
    int __dummyd_Data;
    int __dummyd_PartialHistograms;
    int __dummys_Hist;
    for (int i = 0; i < 16; i += 1) {
        s_Hist[threadIdx.x + (64 * i)] = __dummys_Hist_w();
    }
    __syncthreads();
    for (int pos = 64 * blockIdx.x; pos < dataCount; pos += 279680) {
        if (pos >= (64 * blockIdx.x) + threadIdx.x && pos < dataCount) {
            __dummyd_Data = d_Data[pos];
        }
    }
    __syncthreads();
    if (threadIdx.x < 64) {
        d_PartialHistograms[(64 * blockIdx.x) + threadIdx.x] = __dummyd_PartialHistograms_w();
    }
}
