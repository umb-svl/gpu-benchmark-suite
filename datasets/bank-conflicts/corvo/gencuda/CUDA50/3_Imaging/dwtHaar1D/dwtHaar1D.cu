//pass
//--blockDim=[512,1,1] --gridDim=[1,1,1]

extern __attribute__((device)) int __dummyapprox_final_w();
extern __attribute__((device)) int __dummyid_w();
extern __attribute__((device)) int __dummyod_w();
extern __attribute__((device)) int __dummyshared_w();
__global__ void kernel(int *__dummy, int *approx_final, int *id, int *od, int bdim, int dlevels, int slength_step_half)
{
    extern __shared__ int shared[];
    int __dummyapprox_final;
    int __dummyid;
    int __dummyod;
    int __dummyshared;
    __dummyid = id[(blockIdx.x * (2 * bdim)) + threadIdx.x];
    shared[threadIdx.x] = __dummyshared_w();
    __dummyid = id[((blockIdx.x * (2 * bdim)) + threadIdx.x) + bdim];
    shared[threadIdx.x + bdim] = __dummyshared_w();
    __syncthreads();
    __dummyshared = shared[2 * threadIdx.x];
    __dummyshared = shared[1 + (2 * threadIdx.x)];
    __syncthreads();
    od[((blockIdx.x * bdim) + threadIdx.x) + slength_step_half] = __dummyod_w();
    shared[threadIdx.x + (threadIdx.x >> 4)] = __dummyshared_w();
    __syncthreads();
    if (dlevels > 1) {
        for (int i = 1; i < dlevels; i += 1) {
            if (threadIdx.x < bdim / 2) {
                __dummyshared = shared[(2 * threadIdx.x) + ((2 * threadIdx.x) >> 4)];
                __dummyshared = shared[(1 + (2 * threadIdx.x)) + ((1 + (2 * threadIdx.x)) >> 4)];
                od[((bdim >> 1) + (blockIdx.x * (bdim >> 1))) + threadIdx.x] = __dummyod_w();
                __dummyshared = shared[(2 * threadIdx.x) + ((2 * threadIdx.x) >> 4)];
                __dummyshared = shared[(1 + (2 * threadIdx.x)) + ((1 + (2 * threadIdx.x)) >> 4)];
                shared[(2 * threadIdx.x) + ((2 * threadIdx.x) >> 4)] = __dummyshared_w();
            }
            __syncthreads();
        }
        if (0 == threadIdx.x) {
            __dummyshared = shared[0];
            approx_final[blockIdx.x] = __dummyapprox_final_w();
        }
    }
}
