//pass
//--blockDim=[32,32,1] --gridDim=[12,24,1]

extern __attribute__((device)) int __dummyinput_w();
extern __attribute__((device)) int __dummyoutput_w();
extern __attribute__((device)) int __dummytile_w();
__global__ void kernel(int *__dummy, int *input, int *output, int dimx, int dimy, int dimz)
{
    __shared__ int tile[960];
    int __dummyinput;
    int __dummyoutput;
    int __dummytile;
    for (int i = 2; i > -1; i -= 1) {
        __dummyinput = input[(4 + (4 * (8 + dimx))) + ((((32 * blockIdx.y) + threadIdx.y) * (8 + dimx)) + ((32 * blockIdx.x) + threadIdx.x))];
    }
    __dummyinput = input[(4 + (4 * (8 + dimx))) + ((((32 * blockIdx.y) + threadIdx.y) * (8 + dimx)) + ((32 * blockIdx.x) + threadIdx.x))];
    for (int i1 = 0; i1 < 4; i1 += 1) {
        __dummyinput = input[((4 + (4 * (8 + dimx))) + ((((32 * blockIdx.y) + threadIdx.y) * (8 + dimx)) + ((32 * blockIdx.x) + threadIdx.x))) + ((8 + dimx) * (8 + dimy))];
    }
    for (int iz = 0; iz < dimz; iz += 1) {
        __dummyinput = input[((4 + (4 * (8 + dimx))) + ((((32 * blockIdx.y) + threadIdx.y) * (8 + dimx)) + ((32 * blockIdx.x) + threadIdx.x))) + ((8 + dimx) * (8 + dimy))];
        __syncthreads();
        if (threadIdx.y < 4) {
            __dummyinput = input[(((4 + (4 * (8 + dimx))) + ((((32 * blockIdx.y) + threadIdx.y) * (8 + dimx)) + ((32 * blockIdx.x) + threadIdx.x))) + ((8 + dimx) * (8 + dimy))) - (4 * (8 + dimx))];
            tile[(40 * threadIdx.y) + (4 + threadIdx.x)] = __dummytile_w();
            __dummyinput = input[(((4 + (4 * (8 + dimx))) + ((((32 * blockIdx.y) + threadIdx.y) * (8 + dimx)) + ((32 * blockIdx.x) + threadIdx.x))) + ((8 + dimx) * (8 + dimy))) + (32 * (8 + dimx))];
            tile[(40 * (36 + threadIdx.y)) + (4 + threadIdx.x)] = __dummytile_w();
        }
        if (threadIdx.x < 4) {
            __dummyinput = input[(((4 + (4 * (8 + dimx))) + ((((32 * blockIdx.y) + threadIdx.y) * (8 + dimx)) + ((32 * blockIdx.x) + threadIdx.x))) + ((8 + dimx) * (8 + dimy))) - 4];
            tile[(40 * (4 + threadIdx.y)) + threadIdx.x] = __dummytile_w();
            __dummyinput = input[32 + (((4 + (4 * (8 + dimx))) + ((((32 * blockIdx.y) + threadIdx.y) * (8 + dimx)) + ((32 * blockIdx.x) + threadIdx.x))) + ((8 + dimx) * (8 + dimy)))];
            tile[(40 * (4 + threadIdx.y)) + (36 + threadIdx.x)] = __dummytile_w();
        }
        tile[(40 * (4 + threadIdx.y)) + (4 + threadIdx.x)] = __dummytile_w();
        __syncthreads();
        for (int i2 = 1; i2 < 5; i2 += 1) {
            __dummytile = tile[(40 * ((4 + threadIdx.y) - i2)) + (4 + threadIdx.x)];
            __dummytile = tile[(40 * ((4 + threadIdx.y) + i2)) + (4 + threadIdx.x)];
            __dummytile = tile[(40 * (4 + threadIdx.y)) + ((4 + threadIdx.x) - i2)];
            __dummytile = tile[(40 * (4 + threadIdx.y)) + ((4 + threadIdx.x) + i2)];
        }
        output[((4 + (4 * (8 + dimx))) + ((((32 * blockIdx.y) + threadIdx.y) * (8 + dimx)) + ((32 * blockIdx.x) + threadIdx.x))) + ((8 + dimx) * (8 + dimy))] = __dummyoutput_w();
    }
}
