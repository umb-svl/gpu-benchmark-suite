//pass
//--blockDim=[120,1,1] --gridDim=[1080,1,1]

extern __attribute__((device)) int __dummyimg_w();
extern __attribute__((device)) int __dummyintegral_image_w();
extern __attribute__((device)) int __dummysums_w();
__global__ void kernel(int *__dummy, int *img, int *integral_image)
{
    __shared__ int sums[128];
    int __dummyimg;
    int __dummyintegral_image;
    int __dummysums;
    if (threadIdx.x % 32 == 31) {
        sums[threadIdx.x / 32] = __dummysums_w();
    }
    __syncthreads();
    if (threadIdx.x / 32 == 0) {
        __dummysums = sums[threadIdx.x % 32];
        sums[threadIdx.x % 32] = __dummysums_w();
    }
    __syncthreads();
    if (threadIdx.x / 32 > 0) {
        __dummysums = sums[(threadIdx.x / 32) - 1];
    }
    integral_image[((480 * blockIdx.x) + (threadIdx.x % 4)) + (4 * threadIdx.x)] = __dummyintegral_image_w();
    integral_image[8 + (((480 * blockIdx.x) + ((2 + threadIdx.x) % 4)) + (4 * threadIdx.x))] = __dummyintegral_image_w();
    integral_image[4 + (((480 * blockIdx.x) + (threadIdx.x % 4)) + (4 * threadIdx.x))] = __dummyintegral_image_w();
    integral_image[12 + (((480 * blockIdx.x) + ((2 + threadIdx.x) % 4)) + (4 * threadIdx.x))] = __dummyintegral_image_w();
}
