//pass
//--blockDim=[32,1,1] --gridDim=[1,1,1]

extern __attribute__((device)) int __dummyd_clocks_w();
extern __attribute__((device)) int __dummys_clocks_w();
__global__ void kernel(int *__dummy, int *d_clocks, int N)
{
    __shared__ int s_clocks[32];
    int __dummyd_clocks;
    int __dummys_clocks;
    for (int i = 0; i < N; i += 32) {
        if (i >= threadIdx.x && i < N) {
            __dummyd_clocks = d_clocks[i];
        }
    }
    s_clocks[threadIdx.x] = __dummys_clocks_w();
    __syncthreads();
    for (int i1 = 1; i1 < 17; i1 *= 2) {
        if (threadIdx.x < i1) {
            __dummys_clocks = s_clocks[threadIdx.x];
            __dummys_clocks = s_clocks[threadIdx.x + i1];
            s_clocks[threadIdx.x] = __dummys_clocks_w();
        }
        __syncthreads();
    }
    if (threadIdx.x == 0) {
        __dummys_clocks = s_clocks[0];
        d_clocks[0] = __dummyd_clocks_w();
    }
}
