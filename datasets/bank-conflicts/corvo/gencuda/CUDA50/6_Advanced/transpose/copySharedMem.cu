//pass
//--blockDim=[32,32,1] --gridDim=[64,64,1]

extern __attribute__((device)) int __dummyidata_w();
extern __attribute__((device)) int __dummyodata_w();
extern __attribute__((device)) int __dummytile_w();
__global__ void kernel(int *__dummy, int *idata, int *odata, int height, int nreps, int width)
{
    __shared__ int tile[256];
    int __dummyidata;
    int __dummyodata;
    int __dummytile;
    for (int r = 0; r < nreps; r += 1) {
        for (int i = 0; i < 16; i += 16) {
            if ((16 * blockIdx.x) + threadIdx.x < width && (16 * blockIdx.y) + threadIdx.y < height) {
                __dummyidata = idata[((16 * blockIdx.x) + threadIdx.x) + (width * ((16 * blockIdx.y) + threadIdx.y))];
                tile[(16 * threadIdx.y) + threadIdx.x] = __dummytile_w();
            }
        }
        __syncthreads();
        for (int i1 = 0; i1 < 16; i1 += 16) {
            if ((16 * blockIdx.x) + threadIdx.x < height && (16 * blockIdx.y) + threadIdx.y < width) {
                __dummytile = tile[(16 * threadIdx.y) + threadIdx.x];
                odata[((16 * blockIdx.x) + threadIdx.x) + (width * ((16 * blockIdx.y) + threadIdx.y))] = __dummyodata_w();
            }
        }
    }
}
