//pass
//--blockDim=[256,1,1] --gridDim=[26,1,1]

extern __attribute__((device)) int __dummyd_Buf_w();
extern __attribute__((device)) int __dummyd_Dst_w();
extern __attribute__((device)) int __dummyd_Src_w();
extern __attribute__((device)) int __dummys_Data_w();
__global__ void kernel(int *__dummy, int *d_Buf, int *d_Dst, int *d_Src, int N, int arrayLength)
{
    __shared__ int s_Data[512];
    int __dummyd_Buf;
    int __dummyd_Dst;
    int __dummyd_Src;
    int __dummys_Data;
    if ((256 * blockIdx.x) + threadIdx.x < N) {
        __dummyd_Dst = d_Dst[1023 + (1024 * ((256 * blockIdx.x) + threadIdx.x))];
        __dummyd_Src = d_Src[1023 + (1024 * ((256 * blockIdx.x) + threadIdx.x))];
    }
    s_Data[(2 * threadIdx.x) - (threadIdx.x % arrayLength)] = __dummys_Data_w();
    s_Data[((2 * threadIdx.x) - (threadIdx.x % arrayLength)) + arrayLength] = __dummys_Data_w();
    for (int offset = 1; offset < arrayLength; offset *= 2) {
        __syncthreads();
        __dummys_Data = s_Data[((2 * threadIdx.x) - (threadIdx.x % arrayLength)) + arrayLength];
        __dummys_Data = s_Data[(((2 * threadIdx.x) - (threadIdx.x % arrayLength)) + arrayLength) - offset];
        __syncthreads();
        s_Data[((2 * threadIdx.x) - (threadIdx.x % arrayLength)) + arrayLength] = __dummys_Data_w();
    }
    __dummys_Data = s_Data[((2 * threadIdx.x) - (threadIdx.x % arrayLength)) + arrayLength];
    if ((256 * blockIdx.x) + threadIdx.x < N) {
        d_Buf[(256 * blockIdx.x) + threadIdx.x] = __dummyd_Buf_w();
    }
}
