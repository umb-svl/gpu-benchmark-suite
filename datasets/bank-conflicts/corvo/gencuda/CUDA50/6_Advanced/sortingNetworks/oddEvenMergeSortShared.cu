//pass
//--blockDim=[512,1,1] --gridDim=[1024,1,1]

extern __attribute__((device)) int __dummyd_DstKey_w();
extern __attribute__((device)) int __dummyd_DstVal_w();
extern __attribute__((device)) int __dummyd_SrcKey_w();
extern __attribute__((device)) int __dummyd_SrcVal_w();
extern __attribute__((device)) int __dummys_key_w();
extern __attribute__((device)) int __dummys_val_w();
__global__ void kernel(int *__dummy, int *d_DstKey, int *d_DstVal, int *d_SrcKey, int *d_SrcVal, int __unk1, int __unk2, int arrayLength, int dir)
{
    __shared__ int s_key[1024];
    __shared__ int s_val[1024];
    int __dummyd_DstKey;
    int __dummyd_DstVal;
    int __dummyd_SrcKey;
    int __dummyd_SrcVal;
    int __dummys_key;
    int __dummys_val;
    __dummyd_SrcKey = d_SrcKey[(1024 * blockIdx.x) + threadIdx.x];
    s_key[threadIdx.x] = __dummys_key_w();
    __dummyd_SrcVal = d_SrcVal[(1024 * blockIdx.x) + threadIdx.x];
    s_val[threadIdx.x] = __dummys_val_w();
    __dummyd_SrcKey = d_SrcKey[512 + ((1024 * blockIdx.x) + threadIdx.x)];
    s_key[512 + threadIdx.x] = __dummys_key_w();
    __dummyd_SrcVal = d_SrcVal[512 + ((1024 * blockIdx.x) + threadIdx.x)];
    s_val[512 + threadIdx.x] = __dummys_val_w();
    for (int size = 2; size < 1 + arrayLength; size *= 2) {
        __syncthreads();
        __dummys_key = s_key[(2 * threadIdx.x) - (threadIdx.x % (size / 2))];
        __dummys_val = s_val[(2 * threadIdx.x) - (threadIdx.x % (size / 2))];
        __dummys_key = s_key[((2 * threadIdx.x) - (threadIdx.x % (size / 2))) + (size / 2)];
        __dummys_val = s_val[((2 * threadIdx.x) - (threadIdx.x % (size / 2))) + (size / 2)];
        for (int __unk0 = __unk1; __unk0 < __unk2; __unk0 += 1) {
            __syncthreads();
            if (threadIdx.x % (size / 2) >= size / 2) {
                __dummys_key = s_key[((2 * threadIdx.x) - (threadIdx.x % (size / 2))) - (size / 2)];
                __dummys_val = s_val[((2 * threadIdx.x) - (threadIdx.x % (size / 2))) - (size / 2)];
                __dummys_key = s_key[(2 * threadIdx.x) - (threadIdx.x % (size / 2))];
                __dummys_val = s_val[(2 * threadIdx.x) - (threadIdx.x % (size / 2))];
            }
        }
    }
    __syncthreads();
    __dummys_key = s_key[threadIdx.x];
    d_DstKey[(1024 * blockIdx.x) + threadIdx.x] = __dummyd_DstKey_w();
    __dummys_val = s_val[threadIdx.x];
    d_DstVal[(1024 * blockIdx.x) + threadIdx.x] = __dummyd_DstVal_w();
    __dummys_key = s_key[512 + threadIdx.x];
    d_DstKey[512 + ((1024 * blockIdx.x) + threadIdx.x)] = __dummyd_DstKey_w();
    __dummys_val = s_val[512 + threadIdx.x];
    d_DstVal[512 + ((1024 * blockIdx.x) + threadIdx.x)] = __dummyd_DstVal_w();
}
