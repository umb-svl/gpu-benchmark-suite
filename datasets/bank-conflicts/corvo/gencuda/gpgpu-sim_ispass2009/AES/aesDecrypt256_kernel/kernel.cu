//pass
//--blockDim=[256,1,1] --gridDim=[128,1,1]

extern __attribute__((device)) int __dummyinData_w();
extern __attribute__((device)) int __dummyinvSBoxBlock_w();
extern __attribute__((device)) int __dummyresult_w();
extern __attribute__((device)) int __dummystageBlock1_w();
extern __attribute__((device)) int __dummystageBlock2_w();
extern __attribute__((device)) int __dummytBox0Block_w();
extern __attribute__((device)) int __dummytBox1Block_w();
extern __attribute__((device)) int __dummytBox2Block_w();
extern __attribute__((device)) int __dummytBox3Block_w();
__global__ void kernel(int *__dummy, int *inData, int *result, int inputSize)
{
    __shared__ int invSBoxBlock[256];
    __shared__ int stageBlock1[256];
    __shared__ int stageBlock2[256];
    __shared__ int tBox0Block[256];
    __shared__ int tBox1Block[256];
    __shared__ int tBox2Block[256];
    __shared__ int tBox3Block[256];
    int _unknown_16 = __dummy[threadIdx.x];
    int _unknown_19 = __dummy[threadIdx.x];
    int _unknown_23 = __dummy[threadIdx.x];
    int _unknown_27 = __dummy[threadIdx.x];
    int _unknown_31 = __dummy[threadIdx.x];
    int __dummyinData;
    int __dummyinvSBoxBlock;
    int __dummyresult;
    int __dummystageBlock1;
    int __dummystageBlock2;
    int __dummytBox0Block;
    int __dummytBox1Block;
    int __dummytBox2Block;
    int __dummytBox3Block;
    __dummystageBlock1 = stageBlock1[threadIdx.x];
    __dummyinData = inData[(256 * blockIdx.x) + threadIdx.x];
    __dummytBox0Block = tBox0Block[threadIdx.x];
    __dummytBox1Block = tBox1Block[threadIdx.x];
    __dummytBox2Block = tBox2Block[threadIdx.x];
    __dummytBox3Block = tBox3Block[threadIdx.x];
    __dummyinvSBoxBlock = invSBoxBlock[threadIdx.x];
    __syncthreads();
    __dummystageBlock2 = stageBlock2[threadIdx.x];
    __dummystageBlock1 = stageBlock1[threadIdx.x];
    __syncthreads();
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummytBox0Block = tBox0Block[_unknown_19];
    __dummytBox1Block = tBox1Block[_unknown_23];
    __dummytBox2Block = tBox2Block[_unknown_27];
    __dummytBox3Block = tBox3Block[_unknown_31];
    __dummystageBlock1 = stageBlock1[threadIdx.x];
    __syncthreads();
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummytBox0Block = tBox0Block[_unknown_19];
    __dummytBox1Block = tBox1Block[_unknown_23];
    __dummytBox2Block = tBox2Block[_unknown_27];
    __dummytBox3Block = tBox3Block[_unknown_31];
    __dummystageBlock2 = stageBlock2[threadIdx.x];
    __syncthreads();
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummytBox0Block = tBox0Block[_unknown_19];
    __dummytBox1Block = tBox1Block[_unknown_23];
    __dummytBox2Block = tBox2Block[_unknown_27];
    __dummytBox3Block = tBox3Block[_unknown_31];
    __dummystageBlock1 = stageBlock1[threadIdx.x];
    __syncthreads();
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummytBox0Block = tBox0Block[_unknown_19];
    __dummytBox1Block = tBox1Block[_unknown_23];
    __dummytBox2Block = tBox2Block[_unknown_27];
    __dummytBox3Block = tBox3Block[_unknown_31];
    __dummystageBlock2 = stageBlock2[threadIdx.x];
    __syncthreads();
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummytBox0Block = tBox0Block[_unknown_19];
    __dummytBox1Block = tBox1Block[_unknown_23];
    __dummytBox2Block = tBox2Block[_unknown_27];
    __dummytBox3Block = tBox3Block[_unknown_31];
    __dummystageBlock1 = stageBlock1[threadIdx.x];
    __syncthreads();
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummytBox0Block = tBox0Block[_unknown_19];
    __dummytBox1Block = tBox1Block[_unknown_23];
    __dummytBox2Block = tBox2Block[_unknown_27];
    __dummytBox3Block = tBox3Block[_unknown_31];
    __dummystageBlock2 = stageBlock2[threadIdx.x];
    __syncthreads();
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummytBox0Block = tBox0Block[_unknown_19];
    __dummytBox1Block = tBox1Block[_unknown_23];
    __dummytBox2Block = tBox2Block[_unknown_27];
    __dummytBox3Block = tBox3Block[_unknown_31];
    __dummystageBlock1 = stageBlock1[threadIdx.x];
    __syncthreads();
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummytBox0Block = tBox0Block[_unknown_19];
    __dummytBox1Block = tBox1Block[_unknown_23];
    __dummytBox2Block = tBox2Block[_unknown_27];
    __dummytBox3Block = tBox3Block[_unknown_31];
    __dummystageBlock2 = stageBlock2[threadIdx.x];
    __syncthreads();
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummytBox0Block = tBox0Block[_unknown_19];
    __dummytBox1Block = tBox1Block[_unknown_23];
    __dummytBox2Block = tBox2Block[_unknown_27];
    __dummytBox3Block = tBox3Block[_unknown_31];
    __dummystageBlock1 = stageBlock1[threadIdx.x];
    __syncthreads();
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummytBox0Block = tBox0Block[_unknown_19];
    __dummytBox1Block = tBox1Block[_unknown_23];
    __dummytBox2Block = tBox2Block[_unknown_27];
    __dummytBox3Block = tBox3Block[_unknown_31];
    __dummystageBlock2 = stageBlock2[threadIdx.x];
    __syncthreads();
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummytBox0Block = tBox0Block[_unknown_19];
    __dummytBox1Block = tBox1Block[_unknown_23];
    __dummytBox2Block = tBox2Block[_unknown_27];
    __dummytBox3Block = tBox3Block[_unknown_31];
    __dummystageBlock1 = stageBlock1[threadIdx.x];
    __syncthreads();
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummytBox0Block = tBox0Block[_unknown_19];
    __dummytBox1Block = tBox1Block[_unknown_23];
    __dummytBox2Block = tBox2Block[_unknown_27];
    __dummytBox3Block = tBox3Block[_unknown_31];
    __dummystageBlock2 = stageBlock2[threadIdx.x];
    __syncthreads();
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummystageBlock2 = stageBlock2[_unknown_16 + threadIdx.x];
    __dummytBox0Block = tBox0Block[_unknown_19];
    __dummytBox1Block = tBox1Block[_unknown_23];
    __dummytBox2Block = tBox2Block[_unknown_27];
    __dummytBox3Block = tBox3Block[_unknown_31];
    __dummystageBlock1 = stageBlock1[threadIdx.x];
    __syncthreads();
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummystageBlock1 = stageBlock1[_unknown_16 + threadIdx.x];
    __dummyinvSBoxBlock = invSBoxBlock[_unknown_31];
    __dummystageBlock2 = stageBlock2[threadIdx.x];
    __dummyinvSBoxBlock = invSBoxBlock[_unknown_27];
    __dummystageBlock2 = stageBlock2[threadIdx.x];
    __dummyinvSBoxBlock = invSBoxBlock[_unknown_23];
    __dummystageBlock2 = stageBlock2[threadIdx.x];
    __dummyinvSBoxBlock = invSBoxBlock[_unknown_19];
    __dummystageBlock2 = stageBlock2[threadIdx.x];
    __syncthreads();
    __dummystageBlock2 = stageBlock2[threadIdx.x];
    result[(256 * blockIdx.x) + threadIdx.x] = __dummyresult_w();
}
