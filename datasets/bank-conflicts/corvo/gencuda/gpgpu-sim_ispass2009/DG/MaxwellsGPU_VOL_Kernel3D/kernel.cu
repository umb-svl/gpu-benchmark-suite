//pass
//--blockDim=[32,1,1] --gridDim=[2,1,1]

extern __attribute__((device)) int __dummyg_rhsQ_w();
extern __attribute__((device)) int __dummys_Q_w();
extern __attribute__((device)) int __dummys_facs_w();
__global__ void kernel(int *__dummy, int *g_rhsQ)
{
    __shared__ int s_Q[96];
    __shared__ int s_facs[12];
    int __unk1 = __dummy[threadIdx.x];
    int __unk2 = __dummy[threadIdx.x];
    int __dummyg_rhsQ;
    int __dummys_Q;
    int __dummys_facs;
    s_Q[threadIdx.x] = __dummys_Q_w();
    s_Q[32 + threadIdx.x] = __dummys_Q_w();
    s_Q[64 + threadIdx.x] = __dummys_Q_w();
    s_Q[96 + threadIdx.x] = __dummys_Q_w();
    s_Q[128 + threadIdx.x] = __dummys_Q_w();
    s_Q[160 + threadIdx.x] = __dummys_Q_w();
    if (threadIdx.x < 12) {
        s_facs[threadIdx.x] = __dummys_facs_w();
    }
    __syncthreads();
    for (int __unk0 = __unk1; __unk0 < __unk2; __unk0 += 1) {
        __dummys_Q = s_Q[160 + (threadIdx.x + (96 * blockIdx.x))];
        __dummys_Q = s_Q[192 + (threadIdx.x + (96 * blockIdx.x))];
        __dummys_Q = s_Q[224 + (threadIdx.x + (96 * blockIdx.x))];
        __dummys_Q = s_Q[256 + (threadIdx.x + (96 * blockIdx.x))];
        __dummys_Q = s_Q[288 + (threadIdx.x + (96 * blockIdx.x))];
        __dummys_Q = s_Q[320 + (threadIdx.x + (96 * blockIdx.x))];
        __dummys_Q = s_Q[161 + (threadIdx.x + (96 * blockIdx.x))];
        __dummys_Q = s_Q[193 + (threadIdx.x + (96 * blockIdx.x))];
        __dummys_Q = s_Q[225 + (threadIdx.x + (96 * blockIdx.x))];
        __dummys_Q = s_Q[257 + (threadIdx.x + (96 * blockIdx.x))];
        __dummys_Q = s_Q[289 + (threadIdx.x + (96 * blockIdx.x))];
        __dummys_Q = s_Q[321 + (threadIdx.x + (96 * blockIdx.x))];
    }
    __dummys_facs = s_facs[0];
    __dummys_facs = s_facs[1];
    __dummys_facs = s_facs[2];
    __dummys_facs = s_facs[4];
    __dummys_facs = s_facs[5];
    __dummys_facs = s_facs[6];
    __dummys_facs = s_facs[8];
    __dummys_facs = s_facs[9];
    __dummys_facs = s_facs[10];
    g_rhsQ[threadIdx.x + (96 * blockIdx.x)] = __dummyg_rhsQ_w();
    g_rhsQ[32 + (threadIdx.x + (96 * blockIdx.x))] = __dummyg_rhsQ_w();
    g_rhsQ[64 + (threadIdx.x + (96 * blockIdx.x))] = __dummyg_rhsQ_w();
    g_rhsQ[96 + (threadIdx.x + (96 * blockIdx.x))] = __dummyg_rhsQ_w();
    g_rhsQ[128 + (threadIdx.x + (96 * blockIdx.x))] = __dummyg_rhsQ_w();
    g_rhsQ[160 + (threadIdx.x + (96 * blockIdx.x))] = __dummyg_rhsQ_w();
}
