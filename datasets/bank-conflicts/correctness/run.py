#!/usr/bin/env python3
from pathlib import Path
from os import chdir
import argparse
import yaml
import toml
import subprocess
from tempfile import NamedTemporaryFile
from string import Template
import shlex
import csv
import shutil

def main():
    # Process configuration file and command line arguments:
    with open('config.yaml') as config_file:
        config = yaml.safe_load(config_file)
    args = get_args(config)

    # Initilize results:
    results = dict(map(lambda tool: (tool, list()), args.tool))

    # Benchmark all kernels listed in config.yaml:
    timeout_cmd = ['timeout', str(args.timeout if args.timeout is not None else config['timeout'])]
    for k_yaml in map(Path, config['kernels']):
        if len(args.problem) > 0 and str(k_yaml) not in args.problem:
            continue
        # Read each kernel yaml file for block, grid dims and includes:
        k_data = load_file(k_yaml)
        tool_args = get_tool_args(k_data['block_dim'], k_data['grid_dim'])
        includes = [*map(lambda inc: k_yaml.parent / inc, k_data.get('includes', []))]

        # Loop through all tools being used:
        for (tool, tool_cmd) in config['tools'].items():
            if tool not in args.tool:
                continue

            # Generate a kernel for the current tool:
            kernel = (tool / k_yaml).with_suffix('.c' if tool == 'pug' else '.cu')
            kernel.parent.mkdir(parents=True, exist_ok=True)
            subprocess.run(['kernel-gen.py', str(k_yaml), '-o', str(kernel)] + get_tool(tool))

            # Link includes:
            for include in includes:
                (tool / include).unlink(missing_ok=True)
                shutil.copyfile(Path.cwd() / include, tool / include)

            # Run benchmark:
            with NamedTemporaryFile() as b_txt, NamedTemporaryFile(mode='r') as b_csv:
                b_txt.write(bytes(kernel))
                b_txt.flush()
                subprocess.run(['run-benchmark.py',
                        '-i', b_txt.name,
                        '-l', f"{kernel.parent / kernel.stem}-${{count}}.log",
                        '-o', b_csv.name,
                        '--repeat', str(args.repeat),
                        '--change-dir', # otherwise, gklee can't find includes
                        '-c'] + timeout_cmd + shlex.split(
                            Template(tool_cmd).safe_substitute(tool_args)))
                results[tool].extend(csv.DictReader(b_csv, delimiter=','))

    # Write results out as CSV timing files:
    print()
    for (tool, data) in results.items():
        if not data:
            continue

        fields = list(data[0].keys()) + ["data_races"]

        csv_file = Path(f"timings-{tool}.csv")
        if csv_file.exists():
            with csv_file.open() as b_csv:
                orig_data = dict( (f["filename"], f) for f in csv.DictReader(b_csv, delimiter=','))
            # Update all entries with the new ones
            for f in data:
                orig_data[f["filename"]] = f
                f['data_races'] = ''
            # Replace data by updated data
            data = list(orig_data.values())

        with csv_file.open(mode='w') as csv_fs:
            writer = csv.DictWriter(csv_fs, delimiter=',', fieldnames=fields)
            writer.writeheader()
            writer.writerows(data)
        subprocess.run(['check-races', '--tool', tool, csv_file])
        print(f"{csv_file}: Wrote {len(data)} results.")


def load_file(filename):
    if filename.suffix == ".json":
        import json
        loader = json.load
    elif filename.suffix == ".toml":
        import toml
        loader = toml.load
    else:
        import yaml
        loader = yaml.safe_load
    return loader(filename.open())

# get_tool_args: The only tool that uses {grid, block}_dim is gpuverify, and
# gpuverify requires a single comma-separated string as format.
def get_tool_args(block_dim, grid_dim):
    if isinstance(block_dim, int):
        block_dim = [ block_dim ]
    if isinstance(grid_dim, int):
        grid_dim = [ grid_dim ]

    def format_dim(elem):
        if elem is None:
            return None
        if isinstance(elem, str):
            return elem
        if isinstance(elem, int):
            return str(elem)
        if len(elem) == 0:
            return None
        return ",".join(map(str, elem))

    return dict(
        grid_dim = format_dim(grid_dim),
        block_dim = format_dim(block_dim))

# get_tool: Returns the kernel-gen.py tool format argument for given tool.
def get_tool(tool):
    if tool in ['gpuverify', 'faial', 'pico', 'racuda']:
        return []  # this is the kernel-gen default format.
    if tool in ['sesa', 'simulee']:
        tool = 'gklee'  # sesa is a gklee preprocessor, uses gklee format.
    return ['-t', tool]

# get_args: Parse and validate arguments.
def get_args(config):
    tools = list(config["tools"].keys())
    tools.sort()
    parser = argparse.ArgumentParser(description="Runs verification tools on kernels listed in config.yaml.")
    parser.add_argument('--problem', default=[], action='append', help="Only run a specific problem. Argument can be repeated to run multiple problems. Default: all")
    parser.add_argument('--tool', '-t', choices=tools, default=[], action='append', help="Only run a specific tools. Argument can be repeated to run multiple tools. Default: pico")
    parser.add_argument('--timeout', type=int, help="Set the timeout. Default (90s)")
    parser.add_argument('--repeat', type=int, default=1, help="Repeat each test the given number of times. Default: %(default)s")
    args = parser.parse_args()
    if not args.tool:
        args.tool = ['pico']
    return args

if __name__ == '__main__':
    main()
