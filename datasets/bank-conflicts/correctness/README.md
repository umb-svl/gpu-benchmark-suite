# Correctness tests for bank conflict analysis

Tests are found in the directories:
- `access-patterns/`      -- varying array indexes
- `syntactic-structures/` -- varying ifs and loops

## Running tests

First, ensure your path has been updated by sourcing the file
`../../../env.sh`.  Additionally, ensure that `pico` is in your path;
`pico` can be built from the `umb-svl/faial` source found on GitLab.

The tests can then be run by the commands:
- `./run.py`               -- to run `pico`
- `./run.py --tool racuda` -- to run `racuda`
