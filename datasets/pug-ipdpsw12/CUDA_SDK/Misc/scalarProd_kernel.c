#include "my_cutil.h"

////////////////////////////////////////////////////////////////////////////////
//   Notes for running in PUG:
//   Bitvector size: 16 bits
////////////////////////////////////////////////////////////////////////////////

#define ACCUM_N 16

// We can give symbolic value to ACCUM_N:
//
// extern int ACCUM_N;
//
// However in this case we should try small bitvectors (e.g. nbits = 10)


__global__ void ScalarProdKernel(
    float *d_C,
    float *d_A,
    float *d_B,
    int vectorN,
    int elementN
){
    //Accumulators cache
  __shared__ float accumResult[ACCUM_N];
  
  for (int vec = blockIdx.x; vec < vectorN; vec += gridDim.x) {
    int vectorBase = elementN * vec;
    int vectorEnd  = vectorBase + elementN;
    
    for (int iAccum = threadIdx.x; iAccum < ACCUM_N; iAccum += blockDim.x) {
      float sum = 0;
      
      for (int pos = vectorBase + iAccum; pos < vectorEnd; pos += ACCUM_N)
	sum += d_A[pos] * d_B[pos];
      
      accumResult[iAccum] = sum;
    }
    
    for (int stride = ACCUM_N / 2; stride > 0; stride >>= 1) {
      __syncthreads();
      for (int iAccum = threadIdx.x; iAccum < stride; iAccum += blockDim.x)
	accumResult[iAccum] += accumResult[stride + iAccum];
    }
    
    if(threadIdx.x == 0) d_C[vec] = accumResult[0];
  }
}
