#include "my_cutil.h"
// #define NUM 100

extern __shared__ int sdata[];

int v0, v1, v2, v3;
__shared__ int* g_idata;
__shared__ int* g_odata;


void assume(int i) {
  assert(g_idata[0] == v0);
  assert(g_idata[1] == v1);
  assert(g_idata[2] == v2);
  assert(g_idata[3] == v3);
}


__global__ void kernel()
{
    // load shared mem
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
    sdata[tid] = g_idata[i];
    __syncthreads();

    // for(unsigned int s=blockDim.x/2; s>0; s>>=1)
    unsigned int s = blockDim.x >> 1;

    // if (s > 0)
    {
      if (tid < s)
        {
	  sdata[tid] += sdata[tid + s];
        }
      __syncthreads();
    }

    s = s >> 1;
    // if (s > 0)
    {
      if (tid < s)
        {
	  sdata[tid] += sdata[tid + s];
        }
      __syncthreads();
    }

    // write result for this block to global mem
    if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}

void guarantee() {
  assert(g_odata[0] != v0 + v1 + v2 + v3);
}
