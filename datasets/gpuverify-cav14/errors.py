import csv

def get_error(fp):
    for line in fp:
        if line.endswith('errors generated.\n') or line.endswith('error generated.\n'):
            return 'CU-TO-JSON'
        if line.startswith('Traceback (most recent call last)'):
            return 'FAIAL-INFER'
        if line.startswith('Error parsing '):
            return 'FAIAL-BIN'
        if 'Barrier divergence' in line:
            return 'FAIAL-BIN-ILLFORMED'

def handle(row):
    status = int(row['status'].strip())
    if status == 255:
        with open(row['log']) as fp:
            print(row['filename'], row['log'], get_error(fp))

def main():
    with open("timings.csv") as fp:
        reader = csv.DictReader(fp)
        for row in reader:
            handle(row)

if __name__ == '__main__':
    main()