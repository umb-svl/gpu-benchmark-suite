#include <stdio.h>
#define __requires(x) klee_assume(x)







__global__ void kernel (float* out) {



    out[threadIdx.x + 0 * blockDim.x ] = out[threadIdx.x + 37 * blockDim.x ];
    out[threadIdx.x + 1 * blockDim.x ] = out[threadIdx.x + 3 * blockDim.x ];
    out[threadIdx.x + 2 * blockDim.x ] = out[threadIdx.x + 28 * blockDim.x ];
    out[threadIdx.x + 3 * blockDim.x ] = out[threadIdx.x + 31 * blockDim.x ];
    out[threadIdx.x + 4 * blockDim.x ] = out[threadIdx.x + 37 * blockDim.x ];
    out[threadIdx.x + 5 * blockDim.x ] = out[threadIdx.x + 1 * blockDim.x ];
    out[threadIdx.x + 6 * blockDim.x ] = out[threadIdx.x + 14 * blockDim.x ];
    out[threadIdx.x + 7 * blockDim.x ] = out[threadIdx.x + 30 * blockDim.x ];
    out[threadIdx.x + 8 * blockDim.x ] = out[threadIdx.x + 53 * blockDim.x ];
    out[threadIdx.x + 9 * blockDim.x ] = out[threadIdx.x + 32 * blockDim.x ];

}
int main () {
    
    /* Declare array 'out' */
    float *out;
    cudaMalloc((void**)&out, 4096 * sizeof(float));
    dim3 grid_dim(1);
    dim3 block_dim(64);
    kernel<<< grid_dim, block_dim >>>(
        out
    );
    return 0;
}
