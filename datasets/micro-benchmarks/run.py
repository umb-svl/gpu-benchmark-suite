#!/usr/bin/env python3
import subprocess
import yaml
import argparse
import shlex
from string import Template
from os import path
import re
from tempfile import NamedTemporaryFile
import os

TOOL_2_FORMAT = {
    "faial": "gpuverify",
    "sesa": "gklee",
    "simulee": "gklee",
}

def tool_to_format(tool):
    return TOOL_2_FORMAT.get(tool, tool)


def main():
    params = yaml.safe_load(open("config.yaml"))
    tools = list(params["tools"].keys())
    tools.sort()
    problems = params["problems"]

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="Don't actually run anything, just print out what we are about to do."
    )
    parser.add_argument(
        "--repeat",
        type=int, default=1,
        help="Repeat each test the given number of times. Default: %(default)s"
    )
    parser.add_argument(
        "--problem",
        choices=problems,
    )
    parser.add_argument("--tool", "-t", choices=tools)
    args = parser.parse_args()
    timeout = params["timeout"]
    prob_range = params["range"]
    default_range = params["default_range"]
    for tool in tools:
        if args.tool is not None and tool != args.tool:
            # We want a single tool and it's not this one
            continue
        for prob in problems:
            if args.problem is not None and prob != args.problem:
                continue
            lo, hi = prob_range.get(tool, {}).get(prob, default_range)
            ext = ".cu" if tool != "pug" else ".c"
            input_file = NamedTemporaryFile(delete=False)
            try:
                fmt = tool_to_format(tool)

                for idx in range(lo, hi+1):
                    input_file.write(f"{fmt}/{prob}-{idx}{ext}\n".encode())
                input_file.close()

                tool_cmd = ["timeout", f"{timeout}s"] + shlex.split(params["tools"][tool])
                tool_cmd = list(
                    Template(c).safe_substitute(
                        dict(
                            grid_dim=params["grid_dim"],
                            block_dim=params["block_dim"]
                        )
                    )
                    for c in tool_cmd
                )
                cmd = [
                    "run-benchmark.py",
                    "-i", f"{input_file.name}",
                    "-l", f"logs/{tool}-{prob}-${{count}}.log",
                    "-o", f"timings-{tool}-{prob}.csv",
                    "--repeat", str(args.repeat),
                    "--command",
                ] + tool_cmd
                print("RUN", " ".join(cmd))
                if not args.dry_run:
                    subprocess.run(cmd)
            finally:
                os.unlink(input_file.name)



if __name__ == '__main__':
    main()
