Gklee
* Benchmark: https://github.com/Geof23/GkleeTests
* Hash: 326d5697e4193be744c7c7c3b3b6913be6bb729b  

Simulee
* Source: https://github.com/Lebronmydx/Simulee
* Hash: d8ec3362da9fcfdd649f5ebb4239ff23755c537c

ESBMC-GPU:
* Source: https://raw.githubusercontent.com/ssvlab/ssvlab.github.io/master/gpu/wp-content/uploads/2015/08/cpe2016.zip

SESA:
* Source: https://github.com/lipeng28/SESABench
* Hash: 59cbbcb4f5e79b4a46d5b33400673fe7624a9ef6

PUG:
* Source: http://formalverification.cs.utah.edu/PUG/distributions/pug-v0.2_x64.tar.gz
* Location: Examples/


esbmc-gpu: average sloc 33, max 92
gklee: average sloc 33, max 77
sesa: average sloc 159, max 749
simulee: average sloc 18, max 102
gpuverify: average sloc 42, max 655
