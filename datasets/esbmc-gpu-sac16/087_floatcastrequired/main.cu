#include <call_kernel.h>
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <assert.h>

#define N 4//64


__global__ void foo(float *x, float y) {
	x[threadIdx.x] = __exp10f(y);
}

int main(void){
	float i=2;
	float *A;
	float *dev_A;

	float size= N*sizeof(float);

	A=(float*)malloc(size);

	cudaMalloc((void**)&dev_A, size);

	cudaMemcpy(dev_A, A, size, cudaMemcpyHostToDevice);

	//	foo<<<1,N>>>(dev_A, i);
	ESBMC_verify_kernel_f(foo, 1, N, dev_A, i);

	cudaMemcpy(A, dev_A, size, cudaMemcpyDeviceToHost);

	printf("\n");

	for(int t=0; t<N; t++){
		printf("%.1f ", A[t]);
	}

	cudaFree(dev_A);
	free(A); 

	return 0;
}

