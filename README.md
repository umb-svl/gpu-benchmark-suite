# GPUVerify

* Paper: https://www.doc.ic.ac.uk/~afd/homepages/papers/pdfs/2012/OOPSLA.pdf

Usage:

```
cd tools/gpuverify-2018-03-22
python3 gvtester.py --from-file ../../examples/kernels.txt ../..
```

# Gklee

* Paper: http://formalverification.cs.utah.edu/pdf/PPoPP12-GKLEE-Extended-Version.pdf

# Simulee

* Paper: https://personal.utdallas.edu/~lxz144130/publications/icse2020b.pdf

# ESBMC-GPU

* Paper: https://ssvlab.github.io/lucasccordeiro/papers/cppe2017.pdf

Usage:

```
./tools/esbmc-gpu-2.0/esbmc-gpu ./datasets/esbmc-gpu-cpe16/supported/data-racer/010_test3/main.cu
```

# SESA

* Paper: https://lipeng28.github.io/papers/sc14-sesa.pdf

# PUG

* http://formalverification.cs.utah.edu/PUG/papers/fse10-pug.pdf
* [PUG User Manual](http://formalverification.cs.utah.edu/PUG/docs/pug-user-manual-v0.2.pdf)
* **PUG input requires the following modifications:**
  * Replace **existing** `#include "util.h"` with `#include “my_cutil.h”`.
  * Kernel function names must end with "kernel" or "Kernel".
  * Kernel filenames must contain "kernel" or "Kernel".
  * Kernel file extension must be `.c` or `.C` (for more advance programs).
  * Kernels must cater to ROSE's need, e.g. make explicit type casting on the array arguments of a function call (otherwise ROSE will fail).
  * Include any header files in the same directory as the kernel being verified.

Usage:

```
./tools/pug-v0.2_x64/pug datasets/pug-ipdpsw12/cav/CUDA50/0_Simple/vectorAdd/vectorAddKernel.c
```
