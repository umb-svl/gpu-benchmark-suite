#include "my_cutil.h"

extern __shared__ int s[];

void __global__ kernel (int *out) {
  int id = threadIdx.x;
  int j;

  s[id+2] = 0;
  if (id > 5) {
    bar();
    j++;
    s[id] = 0;
  }
  else {
    bar();
    s[id+1] = 0;
  }

}
