#!/usr/bin/env python3
import subprocess
import shutil
from pathlib import Path
import sys

def change_ext(filename, ext):
    return filename.parent / (filename.stem + ext)

def safe_run(cmd):
    print("RUN", " ".join(map(str, cmd)), file=sys.stderr)
    subprocess.run(cmd, check=True)

def run(filename, args):
    bc = change_ext(filename, ".bc")
    cc = Path(__file__).parent / "gklee-nvcc"
    gklee = Path(__file__).parent / "gklee-bin"
    try:
        safe_run([cc, filename, "-o", bc])
        try:
            safe_run([gklee] + args + [bc])
        finally:
            cpp = Path.cwd() / change_ext(filename, ".cpp").name
            txt = Path.cwd() / change_ext(filename, ".kernelSet.txt").name
            klee_out = [x for x in filename.parent.glob("klee-out-*") if x.is_dir() ]
            to_remove = [
                bc,
                cpp,
                txt,
                Path("flowsGraph.dot"),
                filename.parent / "klee-last",
            ] + klee_out
            for file in to_remove:
                if file.exists():
                    print("DEL", file, file=sys.stderr)
                    if file.is_symlink():
                        file.unlink()
                    elif file.is_dir():
                        shutil.rmtree(file)
                    else:
                        file.unlink()
    except subprocess.CalledProcessError as e:
        sys.exit(e.returncode)
    except KeyboardInterrupt:
        sys.exit(255)

def main():
    import sys
    run(Path(sys.argv[1]), sys.argv[2:])

if __name__ == '__main__':
    main()
