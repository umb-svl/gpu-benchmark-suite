#!/bin/sh

if test $# != 1; then
  echo "An obj dir argument is required for buildobj.sh" >&2;
  exit 1;
fi

SFILE="$1/buildidobj.txt"

echo -n "ESBMC built from " > $SFILE
echo -n 408a1f83585c38437084a6bed2a721789651cabc >> $SFILE
echo -n " " >> $SFILE
echo -n `date` >> $SFILE
echo -n " by " >> $SFILE
echo -n "https://umb-svl.gitlab.io/" >> $SFILE

dd if=/dev/zero bs=c count=1 >> $SFILE;
