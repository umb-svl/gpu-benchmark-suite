source /common.sh
set -x
VERSION=5a1e5080370b9465f030c6b5089bbfa40346ffb9
pkg_install opam git curl

    git clone https://gitlab.com/umb-svl/faial/ &&
    cd faial &&
    git checkout -b dev $VERSION &&
    export PATH="$PATH:/root/.cargo/bin"
    curl https://sh.rustup.rs -sSf | \
        sh -s -- -y --default-toolchain 1.47.0 --profile minimal &&
    cd faial-ui &&
    cargo b --release &&
    cp target/release/faial /usr/local/bin &&
    cd .. &&
    opam init --disable-sandboxing &&
    eval $(opam config env) &&
    opam switch create 4.11.1 &&
    eval $(opam config env) &&
    ./configure.sh -y && \
    make &&
    cp faial-bin /usr/local/bin &&

pkg_remove opam git curl
