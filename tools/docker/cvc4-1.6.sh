apt-get update &&
apt-get install -y \
        antlr3 \
        libantlr3c-dev \
        libboost-thread-dev \
        libreadline-dev \
        pkg-config \
        cxxtest \
        chrpath &&
rm -rf /var/lib/apt/lists/* &&
curl -L -s https://github.com/CVC4/CVC4/archive/1.6.tar.gz -o cvc4.tar.gz &&
tar xf cvc4.tar.gz &&
cd CVC4-1.6 &&
./autogen.sh &&
./configure  &&
make &&
make install &&
rm -rf CVC4-1.6 cvc4.tar.gz

# CVC 1.6
# cvc_conv.cpp: In member function 'virtual smt_ast* cvc_convt::mk_func_app(const smt_sort*, smt_func_kind, const smt_ast* const*, unsigned int)':
# cvc_conv.cpp:131:21: error: 'IFF' is not a member of 'CVC4::kind'
#        e = em.mkExpr(CVC4::kind::IFF, args[0]->e, args[1]->e);
#                      ^
# cvc_conv.cpp: At global scope:
# cvc_conv.cpp:292:56: warning: unused parameter 'sign' [-Wunused-parameter]
#  cvc_convt::mk_smt_bvint(const mp_integer &theint, bool sign, unsigned int w)
#                                                         ^
# cvc_conv.cpp:313:38: warning: unused parameter 'array_subtype' [-Wunused-parameter]
#                             smt_sortt array_subtype)
#                                       ^
