FROM ubuntu:20.04

RUN useradd -m verify

WORKDIR /home/verify

#####################
# Install GKLEE & SESA
USER root
ADD gklee-f77577 /opt/gklee
ENV PATH="$PATH:/opt/gklee"
ADD docker/hello-gklee.cu /home/verify/
RUN apt-get -y update && \
    apt-get install -y \
        python3 \
        libc6-dev \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
# Test config
USER verify
RUN gklee-exec hello-gklee.cu && \
    sesa-exec hello-gklee.cu
#####################

#####################
# Install SIMULEE (depends on GKLEE)
USER root
ADD Simulee /opt/simulee
ENV PATH="$PATH:/opt/simulee"
ADD docker/hello-gklee.cu /home/verify/
RUN apt-get -y update && \
    apt-get install -y \
        python2.7 \
        python-numpy \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
# Test config
USER verify
RUN simulee-exec hello-gklee.cu
#####################

#####################
# Install PUG
USER root
ADD "pug-v0.2_x64" /opt/pug
ENV PATH="$PATH:/opt/pug"
ADD docker/hello-pug.c /home/verify/
USER verify
RUN pug hello-pug.c
#####################

#####################
# Install GPUVerify
USER root
ADD gpuverify-2018-03-22 /opt/gpuverify
ARG DEBIAN_FRONTEND="noninteractive"
ENV TZ="America/New_York"
RUN apt-get -y update && \
    apt-get install -y \
        python \
        python-psutil \
        mono-runtime \
        libtinfo5 \
        libgomp1 \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
ENV PATH="$PATH:/opt/gpuverify"
ADD docker/hello-gv.cu /home/verify/
USER verify
RUN gpuverify --gridDim 1 --blockDim 64 hello-gv.cu
#####################

#####################
# Install ESBMC
USER root
ADD esbmc-gpu-2.0 /opt/esbmc
RUN apt-get -y update && \
    apt-get install -y \
        libz3-4 \
        libc6-dev-i386 \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
ENV PATH="$PATH:/opt/esbmc"
ADD docker/hello-esbmc.cu /home/verify/
USER verify
RUN esbmc-gpu hello-esbmc.cu
#####################

####################
# Install Faial
USER root
ADD faial-dist.tar.bz2 /opt/faial
RUN apt-get -y update && \
    apt-get install -y \
        z3 \
        libllvm10 \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    chmod 755 /opt/faial/bin/faial-infer
ENV PATH="$PATH:/opt/faial/bin"
ADD docker/hello-gv.cu /home/verify/
USER verify
RUN faial hello-gv.cu
#####################
